# coding: UTF-8
import requests
import urllib.request, urllib.error
from bs4 import BeautifulSoup
import lxml.html
import numpy as np
import configparser
import os

os.chdir(os.path.dirname(os.path.abspath(__file__)))
config_ini = configparser.ConfigParser()
config_ini.read('config.ini', encoding='utf-8')
read_default = config_ini['DEFAULT']
userid = read_default.get('User')
psarr1 = int(read_default.get('Passlogic_arr1'))
psarr2 = int(read_default.get('Passlogic_arr2'))
psarr3 = int(read_default.get('Passlogic_arr3'))
psarr4 = int(read_default.get('Passlogic_arr4'))
psarr5 = int(read_default.get('Passlogic_arr5'))
psarr6 = int(read_default.get('Passlogic_arr6'))
psarr7 = int(read_default.get('Passlogic_arr7'))
psarr8 = int(read_default.get('Passlogic_arr8'))

session = requests.session()

login_info = {
    "uid":userid,
    'action':'confirm'
}

# action
url_login = "https://d-ras-w.ddreams.jp/ui/index.php"
res = session.post(url_login, data=login_info)
soup = BeautifulSoup(res.content, 'html.parser')
a2 = soup.find_all("p")

result =  []
bs = np.array([])
count = 0
dammy = ""

for e in a2 :
    if count < 48:
        result.append(e.getText())
        count += 1
else:
    arr = np.array(result)
    #自分で持っているpasslogicの並び
    bs = [arr[psarr1],arr[psarr2],arr[psarr3],arr[psarr4],arr[psarr5],arr[psarr6],arr[psarr7],arr[psarr8]]
    passwd = ','.join(bs)
    print(passwd.replace(",", ""))
