echo "Installing Homebrew"
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

echo "Installing openconnect"
brew install openconnect

echo "Installing vpn-slice"
brew install vpn-slice

echo "install python3"
brew install python3

echo "install Horizon Client"
brew install vmware-horizon-client

mkdir ~/command
mkdir ~/command/org

cd ~/command/org

curl -O https://gitlab.com/lenneth39/splittunnel-kun/-/archive/master/splittunnel-kun-master.zip 
curl -O https://gitlab.com/lenneth39/zsh/-/archive/master/zsh-master.zip
curl -O https://s3-us-west-2.amazonaws.com/secure.notion-static.com/b0035a88-7066-467f-bdd2-e2e2d79ffe1a/DOCOMO-ADCS01-CA.cer
curl -O https://s3-us-west-2.amazonaws.com/secure.notion-static.com/5615eb7f-8bfb-4e18-bcfd-e801f4c0d4f1/DOCOMO-ADCS02-CA.cer
curl -O https://www.bit-drive.ne.jp/support/technical/sra/software/anyconnect-macos-4.10.03104-core-vpn-webdeploy-k9.dmg

unzip ~/command/org/splittunnel-kun-master.zip
unzip ~/command/org/zsh-master.zip

cp -rp splittunnel-kun-master ~/command/stk
cp -rp zsh-master/.zshrc ~/.zshrc
source ~/.zshrc

echo "install python package"
pip3 install beautifulsoup4 certifi chardet idna lxml numpy requests soupsieve urllib3

