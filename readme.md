# SplitTunnel-kun for MacOSのつかいかた

## Intro

- M1 macを買ったら仮想環境がベータ版しかなくて動作不安定だったので作りました。
- スプリットトンネルすることでネイティブのHorizon Clientで動作できるので少々軽くなるかなと。
- 残念ながらWindowsではvpn sliceがないので動かすことができません。。
- ~~ASA側でスプリットトンネル有効にしてほしい~~

---

## 機能

- VPNトンネルとインターネットの併用
- Daas接続のスタティックルートを自動追加
- PasslogicからのOTPを取得し、VPN接続を自動化

---

## 動作確認済みの環境

- MacBook Pro (13-inch, M1, 2020)
- MacOS 11.1

---

## homebrewの準備

- homebrewインストール
    
    `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
    
- homebrewインストール後の確認
`brew doctor`
→Your system is ready to brew.が表示されればOK
- homebrewのバージョン確認
`brew --version`
→バージョンが表示されればOK

---

## sudoerの編集

- `sudo visudo`
    - `[username] ALL=NOPASSWD: ALL`を追加
    - [username]は実行するユーザ名に変更（whoamiで確認できます)

---

## Openconnectのインストール

- openconnectのインストールコマンド
    - `brew install openconnect`
- openconnectインストール後の確認
    - `openconnect -V`
    →バージョンが表示されればOK
    
    ---
    

## **VPN Sliceのインストール**

- VPN Sliceのインストール
    - `brew install vpn-slice`
- VPN Sliceのインストール後の確認
    - `sudo vpn-slice --self-test`
    - →Self-test passed.が表示されればOK

---

## SplitTunnel-kunの設置

- このリンクから[zipファイル](https://gitlab.com/lenneth39/splittunnel-kun/-/archive/master/splittunnel-kun-master.zip)をダウンロード
- もしくは`git clone https://gitlab.com/lenneth39/splittunnel-kun.git`
- どこに置いても起動できますが、構築時は以下のディレクトリで検証しました
    - `mkdir ~/command`

---

## SplitTunnel-kunの初期設定

- `stk/config`配下の`stk.config`を編集します
    - `ID=username@ntt-west.local` ※`username`を自分のものに変更します
    - `VPN_SERVER=d-rasvp-w.ddreams.jp` ※そのままで構いません
    - `DAAS_SERVER=d-rascse-w2px.ddreams.jp` ※自分のDaaSサーバに変更してください
    - `DAAS_Windows10=100.125.0.0/16`  ※以下の表を参考に。

---

### Python3インストール

- python3のインストール、pythonライブラリのインストールが必要です
    
    ```
    brew install python3
    
    pip3 install beautifulsoup4 certifi chardet idna lxml numpy requests soupsieve urllib3
    ```

- config.iniを自分の情報に編集します

## get-passlogic.py用のconfig.iniの書き方

- 自分の認証パターンを以下の対応表に沿って編集します

| 0 | 1 | 2 | 3 |  | 16 | 17 | 18 | 19 |  | 32 | 33 | 34 | 35 |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 4 | 5 | 6 | 7 |  | 20 | 21 | 22 | 23 |  | 36 | 37 | 38 | 39 |
| 8 | 9 | 10 | 11 |  | 24 | 25 | 26 | 27 |  | 40 | 41 | 42 | 43 |
| 12 | 13 | 14 | 15 |  | 28 | 29 | 30 | 31 |  | 44 | 45 | 46 | 47 |
- 例えば、1列目の左上から下に4つ、2列目の左上から下に4つの場合は以下になります。

```
User = xxxxxxx@ntt-west.local
Passlogic_arr1 = 0
Passlogic_arr2 = 4
Passlogic_arr3 = 8
Passlogic_arr4 = 12
Passlogic_arr5 = 1
Passlogic_arr6 = 5
Passlogic_arr7 = 9
Passlogic_arr8 = 13
```


## Daas環境のIPアドレス

Daasに接続できる人はDaas上でipconfigを実行して、自分のWin10のIPアドレスを入れた方がいいです。
---

## SplitTunnel-kun実行

### ターミナルから実行（推奨）

- VPN接続するとき`sudo stk -c`
- VPN切断するとき`sudo stk -d`

### 制限事項

- 自動再接続は多分やってくれると思いますが未確認。

---

## 参考

---
