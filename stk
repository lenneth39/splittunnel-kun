readonly ROOT=$(cd $(dirname $0) && pwd)
readonly NAME="SplitTunnel-Kun For MacOS"
readonly SCRIPT_NAME=$(basename ${0})
readonly CONFIG_FILE="$ROOT/config/stk.config"
readonly PID_FILE="$ROOT/pid/stk.pid"
readonly LOG_FILE="$ROOT/log/stk.log"
readonly GETPASSLOGIC="$ROOT/py/get-passlogic.py"
readonly VERSION=beta9
readonly CONNECTED=0
readonly NOT_CONNECTED=1

error() {
    echo -e "$@" 1>&2
}

abort() {
   echo "$@" 1>&2
   exit 1
}

version() {
    echo "$NAME version $VERSION"
}

usage() {
    cat <<EOF
  _________        .__   .__   __    ___________                                .__ 
 /   _____/______  |  |  |__|_/  |_  \__    ___/ __ __   ____    ____     ____  |  |  
 \_____  \ \____ \ |  |  |  |\   __\   |    |   |  |  \ /    \  /    \  _/ __ \ |  |  
 /        \|  |_> >|  |__|  | |  |     |    |   |  |  /|   |  \|   |  \ \  ___/ |  |__
/_______  /|   __/ |____/|__| |__|     |____|   |____/ |___|  /|___|  /  \___  >|____/
        \/ |__|                                             \/      \/       \/       

`version`

つかいかた:
    $SCRIPT_NAME [command] [<option>]

command:
    -c     VPN接続を開始します
    -d     VPN接続を終了します

option:
    --version, -v        $SCRIPT_NAME のバージョンを表示します
    --help,    -h        ヘルプを表示します（この画面）
    --test,    -t        ConfigのSyntaxチェックを行います（接続はされません）
    --log,     -l        logファイルを表示します
    --status,  -s        取得したIPアドレス、追加したルーティングテーブルを表示します

EOF
}

check_root() {

    if [ "$USER" != "root" ]; then
        echo "[NG] Checking run as root user..."
        echo " -> Please use 'sudo stk -c' to run"
        exit 1
    fi

    echo "[OK] Checking run as root user..."
}

check_config() {

    # failed
    if [ -z "$ID" ]; then
        echo "[NG] Checking config file... ";
        echo "-> ID is empty";
        exit 1
    fi

    if [ -z "$VPN_SERVER" ]; then
        echo "[NG] Checking config file... "
        echo "-> VPN_SERVER is empty"
        exit 1
    fi

    if [ -z "$DAAS_SERVER" ]; then
        echo "[NG] Checking config file... "
        echo "-> DAAS_SERVER is empty"
        exit 1
    fi

    if [ -z "$DAAS_Windows10" ]; then
        echo "[NG] Checking config file... "
        echo "-> DAAS_Windows10 is empty"
        exit 1
    fi

    # success
    echo "[OK] Checking stk.config...";
}

check_config_file() {
    if [ -e "$CONFIG_FILE" ]; then
        echo "[OK] Veryfing path of stk.config... "
    else
        echo "[NG] Veryfing path of stk.config... "
        echo " -> please check “config/stk.config” exists"
        exit 1
    fi
}

is_connected() {
    if [ -e "$PID_FILE" ]; then
        return $CONNECTED
    else
        return $NOT_CONNECTED
    fi
}


connect_vpn() {
    echo 
    echo ======================================================
    echo VPN Server  :"[$VPN_SERVER]"
    echo Username    :"[$ID]"
    echo DaaS Server :"[$DAAS_SERVER]"
    echo Terget Network: "[$DAAS_Windows10]"
    echo ======================================================
    echo 
    echo Connecting... 
    echo  
#   sudo openconnect --user $ID $VPN_SERVER --pid-file $PID_FILE --script "vpn-slice -v --dump $DAAS_SERVER $DAAS_Windows10" --no-dtls -b &> $LOG_FILE
    python3 $GETPASSLOGIC | sudo openconnect --user $ID $VPN_SERVER --passwd-on-stdin --pid-file $PID_FILE --script "vpn-slice -v --dump $DAAS_SERVER $DAAS_Windows10" --no-dtls -b &> $LOG_FILE
}

disconnect() {
    is_connected || abort "[NG]接続されていないようです( *｀ω´)。"
    sudo kill -SIGINT `cat $PID_FILE`
    echo "[OK] VPN Connection has been closed.( ^ω^ )"
}

status() {
    echo "VPNサーバから取得したIPアドレス"
    IPADDR=$(ifconfig -a | grep "100.87")
    echo $IPADDR
    echo  
    echo "VPNを貼った時のルーティングテーブルの一部"
    netstat -rn | grep -e "100." -e "202.19.231."
}

# daasのIPを調べる
check_daas_ip(){
    lsof -i TCP -n -r1 | grep "vmware-" | grep "100."
}

# show log file
log() {
    cat $ROOT/log/stk.log
}

check_connect() {
    is_connected
    if [ $? -eq 0 ]; then
        echo "[NG] Checking established connection..."
        echo " ->Already connected. Please use "sudo stk -d" to disconnect."
        exit 1
    else
        echo "[OK] Checking established connection..."
    fi
}

# checking vpn connection
check_vpn(){
    if [ $? -eq 0 ]; then
        echo "[OK]VPN connection established.( ^ω^ )"
    else
        echo "[NG]VPN Connection failed!( *｀ω´)"
    fi
}

# loading stk.config
load_config(){
    source $CONFIG_FILE
    if [ $? -eq 0 ]; then
        echo "[OK] Loading stk.config... "
    else
        echo "[NG] loading stk.config..."
        echo " ->please check stk.config"
    fi
}


main() {
    case "$1" in
        "-c")

            clear
	        echo 
	        echo "███████╗██████╗ ██╗     ██╗████████╗      ██╗  ██╗██╗   ██╗███╗   ██╗";
            echo "██╔════╝██╔══██╗██║     ██║╚══██╔══╝      ██║ ██╔╝██║   ██║████╗  ██║";
            echo "███████╗██████╔╝██║     ██║   ██║   █████╗█████╔╝ ██║   ██║██╔██╗ ██║";
            echo "╚════██║██╔═══╝ ██║     ██║   ██║   ╚════╝██╔═██╗ ██║   ██║██║╚██╗██║";
            echo "███████║██║     ███████╗██║   ██║         ██║  ██╗╚██████╔╝██║ ╚████║";
            echo "╚══════╝╚═╝     ╚══════╝╚═╝   ╚═╝         ╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝";
            echo 

            echo ======================================================                                             		  
            # checking established connection
            check_connect

            # checking config file exist
	        check_config_file
            
            # loading config file
            load_config

            # validation config file
            check_config

            # checking run as root user
            check_root $1

            # vpn connect
            connect_vpn

            # checkingvpn connect
            check_vpn
            echo ======================================================
	        ;;

        "-d")
	        echo 
	        echo "███████╗██████╗ ██╗     ██╗████████╗      ██╗  ██╗██╗   ██╗███╗   ██╗";
            echo "██╔════╝██╔══██╗██║     ██║╚══██╔══╝      ██║ ██╔╝██║   ██║████╗  ██║";
            echo "███████╗██████╔╝██║     ██║   ██║   █████╗█████╔╝ ██║   ██║██╔██╗ ██║";
            echo "╚════██║██╔═══╝ ██║     ██║   ██║   ╚════╝██╔═██╗ ██║   ██║██║╚██╗██║";
            echo "███████║██║     ███████╗██║   ██║         ██║  ██╗╚██████╔╝██║ ╚████║";
            echo "╚══════╝╚═╝     ╚══════╝╚═╝   ╚═╝         ╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝";
            echo 

            check_root $1
            disconnect
            ;;

        "-h" | "--help")
            usage
            ;;

        "-v" | "--version")
            version
            ;;

        "-t" | "--test")
            echo 
            check_config_file
            load_config
            check_config
            echo 
            ;;

        "--status" | "-s")
            status
            ;;
        "--check" | "-ch")
            check_daas_ip
            ;;
        
        "--log" | "-l")
            log
            ;;
        *)
            usage
            ;;
    esac
    exit 0
}

main $@
